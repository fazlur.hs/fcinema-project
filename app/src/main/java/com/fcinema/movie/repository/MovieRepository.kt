package com.fcinema.movie.repository

import com.fcinema.movie.api.MovieDBAPI
import com.fcinema.movie.api.RequestAPI

enum class MoviesCategory {
    UPCOMING, NOW_PLAYING, POPULAR, TOP_RATED
}

class MovieRepository(private val api: MovieDBAPI) : RequestAPI() {
    suspend fun getMovies(category: MoviesCategory = MoviesCategory.UPCOMING, page: Int = 1) =
        apiRequest {
            when (category) {
                MoviesCategory.UPCOMING -> api.getUpcomingMovies(page)
                MoviesCategory.NOW_PLAYING -> api.getNowPlayingMovies(page)
                MoviesCategory.POPULAR -> api.getPopularMovies(page)
                MoviesCategory.TOP_RATED -> api.getTopRatedMovies(page)
            }
        }

    suspend fun getMovieDetail(movieId: String) = apiRequest { api.getMovieDetail(movieId) }
    suspend fun getMovieReviews(movieId: String) = apiRequest { api.getMovieReviews(movieId) }
}