package com.fcinema.movie.repository

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.fcinema.movie.ui.detail.MovieDetailViewModel
import com.fcinema.movie.ui.list.MovieListViewModel


@Suppress("UNCHECKED_CAST")
class ViewModelFactory(
    private val repository: MovieRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieListViewModel::class.java))
            return MovieListViewModel(repository) as T
        else if (modelClass.isAssignableFrom(MovieDetailViewModel::class.java))
            return MovieDetailViewModel(repository) as T
        throw IllegalArgumentException()
    }

}