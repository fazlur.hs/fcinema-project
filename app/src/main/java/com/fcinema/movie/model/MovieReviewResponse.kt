package com.fcinema.movie.model

data class MovieReviewResponse(
    val id: Int,
    val page: Int,
    val results: List<MovieReview>,
    val total_pages: Int,
    val total_results: Int
)