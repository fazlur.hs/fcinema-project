package com.fcinema.movie.model

data class Genre(
    val id: Int,
    val name: String
)