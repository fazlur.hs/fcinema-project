package com.fcinema.movie.model

data class Dates(
    val maximum: String,
    val minimum: String
)