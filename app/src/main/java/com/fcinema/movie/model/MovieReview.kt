package com.fcinema.movie.model

data class MovieReview(
    val author: String,
    val content: String,
    val id: String,
    val url: String
)