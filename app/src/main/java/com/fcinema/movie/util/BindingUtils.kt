package com.fcinema.movie.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.fcinema.movie.api.BASE_URL

@BindingAdapter("image")
fun loadImage(view: ImageView, url: String) {
    Glide.with(view)
        .load("https://image.tmdb.org/t/p/w185/$url")
        .into(view)
}