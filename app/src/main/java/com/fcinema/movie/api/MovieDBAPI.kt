package com.fcinema.movie.api

import com.fcinema.movie.model.Movie
import com.fcinema.movie.model.MovieListResponse
import com.fcinema.movie.model.MovieReviewResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

const val BASE_URL = "https://api.themoviedb.org"
const val QUERY_API_KEY = "8769cdf333ac8d674f7da32d1afcef9d"
const val QUERY_LANG = "en"

interface MovieDBAPI {

    @GET("/3/movie/upcoming?api_key=$QUERY_API_KEY&language=$QUERY_LANG-US")
    suspend fun getUpcomingMovies(@Query("page") page: Int = 1): Response<MovieListResponse>

    @GET("/3/movie/now_playing?api_key=$QUERY_API_KEY&language=$QUERY_LANG-US")
    suspend fun getNowPlayingMovies(@Query("page") page: Int = 1): Response<MovieListResponse>

    @GET("/3/movie/popular?api_key=$QUERY_API_KEY&language=$QUERY_LANG-US")
    suspend fun getPopularMovies(@Query("page") page: Int = 1): Response<MovieListResponse>

    @GET("/3/movie/top_rated?api_key=$QUERY_API_KEY&language=$QUERY_LANG-US")
    suspend fun getTopRatedMovies(@Query("page") page: Int = 1): Response<MovieListResponse>

    @GET("/3/movie/{movieId}?api_key=$QUERY_API_KEY&language=en-US")
    suspend fun getMovieDetail(@Path("movieId") movieId: String): Response<Movie>

    @GET("/3/movie/{movieId}/reviews?api_key=$QUERY_API_KEY&language=en-US")
    suspend fun getMovieReviews(@Path("movieId") movieId: String): Response<MovieReviewResponse>

    companion object {
        operator fun invoke(): MovieDBAPI {
            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(MovieDBAPI::class.java)
        }
    }
}