package com.fcinema.movie.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fcinema.movie.model.Movie
import com.fcinema.movie.model.MovieReview
import com.fcinema.movie.repository.MovieRepository
import com.fcinema.movie.repository.MoviesCategory
import com.fcinema.movie.util.Coroutines
import kotlinx.coroutines.Job

class MovieDetailViewModel(
    private val repository: MovieRepository
) : ViewModel() {

    private lateinit var job: Job

    private val _movie = MutableLiveData<Movie>()
    val movie: LiveData<Movie>
        get() = _movie

    private val _reviews = MutableLiveData<List<MovieReview>>()
    val reviews: LiveData<List<MovieReview>>
        get() = _reviews

    fun getMovieDetail(movieId: String) {
        job = Coroutines.ioThenMain(
            { repository.getMovieDetail(movieId) },
            {
                if (it != null) {
                    _movie.value = it
                }
            }
        )
        job = Coroutines.ioThenMain(
            { repository.getMovieReviews(movieId) },
            {
                if (it != null) {
                    _reviews.value = it.results
                }
            }
        )
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}