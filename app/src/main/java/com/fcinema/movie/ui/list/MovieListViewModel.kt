package com.fcinema.movie.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fcinema.movie.model.Movie
import com.fcinema.movie.repository.MovieRepository
import com.fcinema.movie.repository.MoviesCategory
import com.fcinema.movie.util.Coroutines
import kotlinx.coroutines.Job

class MovieListViewModel(
    private val repository: MovieRepository
) : ViewModel() {

    private lateinit var job: Job

    private val _movies = MutableLiveData<List<Movie>>()
    val movies: LiveData<List<Movie>>
        get() = _movies

    fun getMovies(category: MoviesCategory = MoviesCategory.UPCOMING, page: Int = 1) {
        job = Coroutines.ioThenMain(
            { repository.getMovies(category, page) },
            {
                if (it != null) {
                    _movies.value = it.results
                }
            }
        )
    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}