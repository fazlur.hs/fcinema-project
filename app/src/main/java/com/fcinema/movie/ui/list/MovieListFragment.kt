package com.fcinema.movie.ui.list

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fcinema.movie.R
import com.fcinema.movie.api.MovieDBAPI
import com.fcinema.movie.model.Movie
import com.fcinema.movie.repository.MovieRepository
import com.fcinema.movie.repository.MoviesCategory
import com.fcinema.movie.repository.ViewModelFactory
import kotlinx.android.synthetic.main.movies_list_fragment.*
import kotlinx.android.synthetic.main.movies_list_fragment.view.*

class MovieListFragment : Fragment(), MovieItemClickListener, CategoryBottomSheetListener {

    private lateinit var factory: ViewModelFactory
    private lateinit var viewModel: MovieListViewModel
    private var selectedCategory = MoviesCategory.POPULAR

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(
            R.layout.movies_list_fragment,
            container,
            false
        )
        view.btnCategory.setOnClickListener() {
            val categorySheet = CategoryBottomSheetDialog(this)
            categorySheet.show(parentFragmentManager, "Category Sheet")
        }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.movie_list_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val action = MovieListFragmentDirections.actionListToFavorite()
        findNavController().navigate(action)
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val api = MovieDBAPI()
        val repository = MovieRepository(api)
        val adapter = MovieListAdapter(this)
        recyclerViewMovies.adapter = adapter
        recyclerViewMovies.layoutManager = LinearLayoutManager(requireContext())
        recyclerViewMovies.setHasFixedSize(true)

        factory = ViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(MovieListViewModel::class.java)

        viewModel.getMovies(selectedCategory)

        viewModel.movies.observe(viewLifecycleOwner, Observer { movies ->
            adapter.updateData(movies)
        })
    }

    override fun onMovieViewItemClick(view: View, movie: Movie) {
        val action = MovieListFragmentDirections.actionListToDetail(movie)
        action.title = movie.title
        findNavController().navigate(action)
    }

    override fun onSelectedCategory(view: View, category: MoviesCategory) {
        selectedCategory = category
        viewModel.getMovies(category)
    }
}