package com.fcinema.movie.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.fcinema.movie.BaseApplication
import com.fcinema.movie.R
import com.fcinema.movie.api.MovieDBAPI
import com.fcinema.movie.model.Movie
import com.fcinema.movie.repository.MovieRepository
import com.fcinema.movie.repository.ViewModelFactory
import kotlinx.android.synthetic.main.movie_detail_fragment.*

class MovieDetailFragment : Fragment() {

    private lateinit var factory: ViewModelFactory
    private lateinit var viewModel: MovieDetailViewModel
    private var isFavorite: Boolean = false

    private val args: MovieDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.movie_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val api = MovieDBAPI()
        val repository = MovieRepository(api)

        factory = ViewModelFactory(repository)
        viewModel = ViewModelProviders.of(this, factory).get(MovieDetailViewModel::class.java)

        val movie = args.movie
        if (movie != null) {
            viewModel.getMovieDetail("${movie.id}")
            showData(movie)
        }

        viewModel.reviews.observe(viewLifecycleOwner, Observer { reviews ->
            recyclerViewReviewMovies.also {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter = MovieReviewListAdapter(reviews)
            }
        })
    }

    private fun showData(movie: Movie) {
        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w185/${movie.poster_path}")
            .into(imgPoster)
        txtTitle.text = movie.title
        txtReleaseDate.text = movie.release_date
        txtOverview.text = movie.overview

        var apps = activity?.application as BaseApplication
        isFavorite = apps.isFavoriteMovie(movie)
        imgFav.setImageResource(if(isFavorite) R.drawable.heart_fav else R.drawable.heart_outline)

        imgFav.setOnClickListener() {
            isFavorite = !isFavorite
            imgFav.setImageResource(if(isFavorite) R.drawable.heart_fav else R.drawable.heart_outline)
            if(isFavorite) {
                apps.addFavoriteMovie(movie)
            }
            else {
                apps.removeFavoriteMovie(movie)
            }
        }
    }
}