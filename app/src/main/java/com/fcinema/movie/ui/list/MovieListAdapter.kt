package com.fcinema.movie.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fcinema.movie.R
import com.fcinema.movie.databinding.MovieItemBinding
import com.fcinema.movie.model.Movie

class MovieListAdapter(
    private val listener: MovieItemClickListener
) : RecyclerView.Adapter<MovieListAdapter.MoviesViewHolder>() {

    private var movies = mutableListOf<Movie>()

    override fun getItemCount() = movies.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MoviesViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.movie_item,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.recyclerviewMovieBinding.movie = movies[position]
        holder.recyclerviewMovieBinding.cardMovie.setOnClickListener {
            listener.onMovieViewItemClick(
                holder.recyclerviewMovieBinding.cardMovie,
                movies[position]
            )
        }
    }

    fun updateData(newMovies: List<Movie>) {
        movies.clear()
        movies.addAll(newMovies)
        notifyDataSetChanged()
    }

    inner class MoviesViewHolder(
        val recyclerviewMovieBinding: MovieItemBinding
    ) : RecyclerView.ViewHolder(recyclerviewMovieBinding.root)
}