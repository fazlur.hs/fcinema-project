package com.fcinema.movie.ui.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.fcinema.movie.R
import com.fcinema.movie.databinding.MovieReviewItemBinding
import com.fcinema.movie.model.MovieReview

class MovieReviewListAdapter(
    private val reviews: List<MovieReview>
) : RecyclerView.Adapter<MovieReviewListAdapter.MovieReviewViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MovieReviewListAdapter.MovieReviewViewHolder =
        MovieReviewViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.movie_review_item,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = reviews.size

    override fun onBindViewHolder(
        holder: MovieReviewListAdapter.MovieReviewViewHolder,
        position: Int
    ) {
        holder.recyclerviewMovieReviewBinding.review = reviews[position]
    }

    inner class MovieReviewViewHolder(
        val recyclerviewMovieReviewBinding: MovieReviewItemBinding
    ) : RecyclerView.ViewHolder(recyclerviewMovieReviewBinding.root)
}