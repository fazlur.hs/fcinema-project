package com.fcinema.movie.ui.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.fcinema.movie.BaseApplication
import com.fcinema.movie.R
import com.fcinema.movie.api.MovieDBAPI
import com.fcinema.movie.model.Movie
import com.fcinema.movie.repository.MovieRepository
import com.fcinema.movie.repository.ViewModelFactory
import com.fcinema.movie.ui.list.*
import kotlinx.android.synthetic.main.movies_list_fragment.*
import kotlinx.android.synthetic.main.movies_list_fragment.view.*
import kotlinx.android.synthetic.main.movies_list_fragment.view.recyclerViewMovies

class FavoriteListFragment : Fragment(), MovieItemClickListener {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.favorite_list_fragment,
            container,
            false
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        val adapter = MovieListAdapter(this)
        recyclerViewMovies.adapter = adapter
        recyclerViewMovies.layoutManager = LinearLayoutManager(requireContext())
        recyclerViewMovies.setHasFixedSize(true)

        var apps = activity?.application as BaseApplication
        adapter.updateData(apps.getMovies())
    }

    override fun onMovieViewItemClick(view: View, movie: Movie) {
        val action = FavoriteListFragmentDirections.actionFavoriteToDetail(movie)
        action.title = movie.title
        findNavController().navigate(action)
    }
}