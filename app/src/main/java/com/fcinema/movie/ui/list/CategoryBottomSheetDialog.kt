package com.fcinema.movie.ui.list

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fcinema.movie.R
import com.fcinema.movie.repository.MoviesCategory
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.movie_list_bottom_sheet.view.*
import java.lang.ClassCastException

interface CategoryBottomSheetListener {
    fun onSelectedCategory(view: View, category: MoviesCategory)
}

class CategoryBottomSheetDialog(private var listener: CategoryBottomSheetListener) :
    BottomSheetDialogFragment(), View.OnClickListener {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.movie_list_bottom_sheet, container, false)
        view.btnUpcoming.setOnClickListener(this)
        view.btnNowPlaying.setOnClickListener(this)
        view.btnTopRated.setOnClickListener(this)
        view.btnPopular.setOnClickListener(this)
        return view
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                R.id.btnPopular -> v?.let {
                    listener.onSelectedCategory(
                        it,
                        MoviesCategory.POPULAR
                    )
                }
                R.id.btnUpcoming -> v?.let {
                    listener.onSelectedCategory(
                        it,
                        MoviesCategory.UPCOMING
                    )
                }
                R.id.btnTopRated -> v?.let {
                    listener.onSelectedCategory(
                        it,
                        MoviesCategory.TOP_RATED
                    )
                }
                R.id.btnNowPlaying -> v?.let {
                    listener.onSelectedCategory(
                        it,
                        MoviesCategory.NOW_PLAYING
                    )
                }
            }
        }
        dismiss()
    }
}