package com.fcinema.movie.ui.list

import android.view.View
import com.fcinema.movie.model.Movie

interface MovieItemClickListener {
    fun onMovieViewItemClick(view: View, movie: Movie)
}