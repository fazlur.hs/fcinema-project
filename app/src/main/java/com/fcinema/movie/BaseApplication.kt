package com.fcinema.movie

import android.app.Application
import android.os.Build
import androidx.annotation.RequiresApi
import com.fcinema.movie.model.Movie

class BaseApplication : Application() {

    private var movies = mutableListOf<Movie>()

    fun getMovies() = movies

    fun isFavoriteMovie(movie: Movie) : Boolean {
        for(m in movies) if(m.id == movie.id) return true
        return false
    }

    fun addFavoriteMovie(movie: Movie) {
        movies.add(movie)
    }

    fun removeFavoriteMovie(movie: Movie) {
        var index = -1;
        for((i, value) in movies.withIndex()) if(value.id == movie.id) {
            index = i
            break
        }
        movies.removeAt(index)
    }
}